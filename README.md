# jQuery Example Project

## What is it?

  The purpose of this project is to try and keep code of using jQuery or jQuery UI.
  And all codes will be able to run on local (with web server).

## Resource

  * [jQuery](https://api.jquery.com/)
  * [jQuery UI Api](https://api.jqueryui.com/)
  * [jQuery UI Demo](https://jqueryui.com/)
