# jQuery UI Autocomplete Example

## What is it?

  Example of using autocomplete in local. 

## Resource

  * [Demo](https://jqueryui.com/autocomplete/)
  * [Api](https://api.jqueryui.com/autocomplete/)

## How to use?

  * `searchAutocomplete.php` is the remoteSource.
  * `autocomplete_customdisplay.html` - Custom dropdown display, local data.
  * `autocomplete_remoteSource.html` - Get data from the remoteSource.
